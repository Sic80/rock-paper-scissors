import { FC, useState } from "react";
import { ActionTypes } from "./constants/ActionTypes";

interface SwitchProps {
    dispatch: (e: any) => void;
}
 
const Switch: FC<SwitchProps> = ({ dispatch }) => {

    const [positionClicked, setPositionClicked] = useState<number>(0);

    return ( 
        <div className="switch_container">                                       
            <div 
                className='switch0'
                onClick={() => {
                    setPositionClicked(0);
                    dispatch({type: ActionTypes.SET_IS_LIZARD_SPOCK});
                }}
            >    
                    <div className={`switch_circle0 ${positionClicked === 1 && 'animation0'} ${positionClicked === 0 && 'animation1'}`}></div>    
            </div> 
            <div 
                className='switch1'
                onClick={() => {
                    setPositionClicked(1);
                    dispatch({type: ActionTypes.SET_IS_LIZARD_SPOCK});                
                }}
            >    
                    
            </div>    
        </div>
     );
}
 
export default Switch;