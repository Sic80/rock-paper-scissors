import ShapeButton from "./ShapeButton";
import { ReactComponent as Paper } from '../pic/icon-paper.svg';
import { ReactComponent as Scissors } from '../pic/icon-scissors.svg';
import { ReactComponent as Rock } from '../pic/icon-rock.svg';
import { Dispatch } from "react";
import { Action } from "./types/reducerActionType";

export interface ChooseShapeTriangleProps {
    dispatch: Dispatch<Action>;
}
 
const ChooseShapeTriangle: React.FunctionComponent<ChooseShapeTriangleProps> = ({ dispatch }) => {
    return ( 
        <div className="triangle-shapes_container">
            <div className="shapes_container-top"> 
                <div className="triangle_tl">
                    <ShapeButton 
                        dispatch={dispatch}
                        child={<Paper viewBox="-20 -15 90 90" width="90%"/>}
                        shapeName="paper"
                        size="large"

                    />
                </div>
                <div className="triangle_tr">
                    <ShapeButton 
                        dispatch={dispatch}
                        child={<Scissors viewBox="-20 -15 90 90" width="90%"/>}
                        shapeName="scissors"
                        size="large"
                    />
                </div>
            </div>
            <div className="triangle_b">
                <ShapeButton 
                    dispatch={dispatch}
                    child={<Rock viewBox="-20 -20 90 90" width="90%"/>}
                    shapeName="rock"
                    size="large"
                />  
            </div>
        </div>
     );
}
 
export default ChooseShapeTriangle;