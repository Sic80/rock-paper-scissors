import React from "react";
import { Dispatch } from "react";
import { handleClick } from "./HandleClick";
import { Action } from "./types/reducerActionType";

export interface ShapeButtonProps {
    child: React.ReactNode;
    dispatch?: Dispatch<Action>;
    shapeName: string;
    isLizardSpock?: boolean | undefined;
    size: string;
}
 
const ShapeButton: React.FunctionComponent<ShapeButtonProps> = ({ child, dispatch, shapeName, isLizardSpock, size }) => {

    return ( 
        <button 
            className={`shape-button-${size} ${shapeName}-${size}`}
            onClick={() => dispatch && handleClick(shapeName, isLizardSpock, dispatch)}
        >
            {child}   
        </button>
     );
}
 
export default ShapeButton;