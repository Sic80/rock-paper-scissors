import ShapeButton from "./ShapeButton";
import { ReactComponent as Paper } from '../pic/icon-paper.svg';
import { ReactComponent as Scissors } from '../pic/icon-scissors.svg';
import { ReactComponent as Rock } from '../pic/icon-rock.svg';
import { ReactComponent as Lizard } from '../pic/icon-lizard.svg';
import { ReactComponent as Spock } from '../pic/icon-spock.svg';
import { Dispatch } from "react";
import { Action } from "./types/reducerActionType";


export interface ChooseShapePentagonProps {
    dispatch: Dispatch<Action>;
}
 
const ChooseShapePentagon: React.FunctionComponent<ChooseShapePentagonProps> = ({ dispatch }) => {
    
    return ( 
        <div className="pentagon-shapes_container">
            <div className="pentagon_top">
                <div className="pentagon_tl">
                    <ShapeButton     
                        dispatch={dispatch}
                        child={<Spock viewBox="-27 -15 90 90" width="100%"/>}
                        shapeName="spock"
                        isLizardSpock={true}  
                        size="small"
                    />
                </div>
                <div className="pentagon_tc">
                    <ShapeButton           
                        dispatch={dispatch}
                        child={<Scissors viewBox="-20 -15 90 90" />}
                        shapeName="scissors"
                        isLizardSpock={true}
                        size="small"
                    />
                </div>
                <div className="pentagon_tr">
                    <ShapeButton                               
                        dispatch={dispatch}
                        child={<Paper viewBox="-20 -15 90 90" width="100%"/>}
                        shapeName="paper"
                        isLizardSpock={true}
                        size="small"
                    />
                </div>
            </div>
            <div className="pentagon_bottom">
                <div className="pentagon_bl">
                    <ShapeButton                               
                        dispatch={dispatch}
                        child={<Lizard viewBox="-15 -15 90 90" width="100%"/>}
                        shapeName="lizard"
                        isLizardSpock={true}
                        size="small"
                    />
                </div>
                <div className="pentagon_br">
                    <ShapeButton                               
                        dispatch={dispatch}
                        child={<Rock viewBox="-20 -20 90 90" />}
                        shapeName="rock"
                        isLizardSpock={true}
                        size="small"
                    />
                </div>
            </div>                       
        </div>
    );
}
 
export default ChooseShapePentagon;