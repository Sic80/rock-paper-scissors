export  enum Winner {
    DRAW = "DRAW",
    PLAYER = "PLAYER_WON",
    CPU = "CPU_WON"
}