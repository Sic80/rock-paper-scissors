import { InitialState } from './types/initialStateInterface';


export interface TitleAndScoreBoxProps {
    state: InitialState;   
}
 
const TitleAndScoreBox: React.FunctionComponent<TitleAndScoreBoxProps> = ({ state }) => {
    
    const { winCounter, isLizardSpock } = state;

    return ( 
        <div className="title-score_box" >
            <div className={`title_box ${isLizardSpock && 'spock-title_box'}`}>
                <p>ROCK</p>
                <p> PAPER</p>
                <p> SCISSORS</p>
                {isLizardSpock && 
                    <div>
                        <p>LIZARD</p>
                        <p>SPOCK</p>
                    </div>
                }
            </div>
            <div className="score_box">
                <p className="score_text">
                    SCORE
                </p>
                <p className="score_number">
                    {winCounter}                            
                </p>
            </div>
        </div>
    );
}
 
export default TitleAndScoreBox;