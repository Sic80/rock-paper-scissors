import { ReactComponent as RulesImage } from '../pic/triangle-info.svg';
import { ReactComponent as PentagonRulesImage } from '../pic/pentagon-info.svg';
import { ReactComponent as CloseIcon } from '../pic/close.svg';
import { Dispatch } from 'react';
import { SetIsPanelOpen } from './types/SetIsPanelOpenInterface';
import { ActionTypes } from './constants/ActionTypes';
import SlidingPanel from 'react-sliding-side-panel';
import "react-sliding-pane/dist/react-sliding-pane.css";
import { InitialState } from './types/initialStateInterface';


export interface RulesButtonAndPanelProps {
    state: InitialState;
    dispatch: Dispatch<SetIsPanelOpen>;
}
 
const RulesButtonAndPanel: React.FunctionComponent<RulesButtonAndPanelProps> = ({ state, dispatch }) => {
  
    const { isPanelOpen, isLizardSpock } = state;

    return ( 
        <div className="btn-pnl-cont">
            <button 
                className="rules-button"
                onClick={() => dispatch({type: ActionTypes.SET_IS_PANEL_OPEN})}
            >
                RULES
            </button>

            <SlidingPanel
                type={'bottom'}
                isOpen={isPanelOpen}
                size={100}
            >
                <div className="modal_container">
                    <div className={`${isLizardSpock ? 'modal-info-pent' : 'modal-info'}`}>
                        <p className="info-text">
                            RULES
                        </p> 
                        <div 
                            className="modal-close-button"
                            onClick={() => dispatch({type: ActionTypes.SET_IS_PANEL_OPEN})}
                        >
                            <CloseIcon className="close-icon"/>
                        </div>                       
                            {isLizardSpock ? (
                                <div className="modal-img-container-pent">
                                    <PentagonRulesImage className="pentagon"/>
                                </div>
                            ) : (
                                <div className="modal-img_container">
                                    <RulesImage className="triangle"/>
                                </div>
                            )}
                    </div>                    
                </div>
            </SlidingPanel>

        </div> 
     );
}
 
export default RulesButtonAndPanel;