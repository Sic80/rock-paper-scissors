import { Dispatch } from 'react';
import { ActionTypes } from './constants/ActionTypes';
import { Winner } from './constants/Winner';
import { Action } from './types/reducerActionType';


export function handleClick(shape: string, isLizardSpock: boolean | undefined, dispatch: Dispatch<Action> ) {
        
    function getRandomShape(): string {
        let shapes: string[] =  isLizardSpock ? ["rock", "paper", "scissors", "lizard", "spock"] : ["rock", "paper", "scissors"];
        const randomIdx: number = Math.floor(Math.random() * shapes.length);
        return shapes[randomIdx];
    };

    function theWinnerIs(playerShape: string, cpuShape: string): string {
        if (playerShape === cpuShape) {
            return Winner.DRAW;
        } else {
            if ((playerShape === "rock" && ((cpuShape === "scissors" || cpuShape === "lizard"))) || (playerShape === "scissors" && ((cpuShape === "paper" || cpuShape === "lizard"))) || (playerShape === "paper" && ((cpuShape === "rock" || cpuShape === "spock"))) ||
                (playerShape === "lizard" && ((cpuShape === "spock" || cpuShape === "paper"))) || (playerShape === "spock" && ((cpuShape === "scissors" || cpuShape === "rock")))) {
                dispatch({type: ActionTypes.INCREMENT_WIN_COUNTER});
                return Winner.PLAYER;
            } else {
                dispatch({type: ActionTypes.DECREMENT_WIN_COUNTER});
                return Winner.CPU;
            }
        }
    };

    dispatch({type: ActionTypes.SET_PLAYER_SHAPE, payload: shape});
    const randomShape = getRandomShape();
    setTimeout(() => 
        dispatch({type: ActionTypes.SET_CPU_SHAPE, payload: randomShape}), 
        3000
    );
    setTimeout(() =>
      dispatch({type: ActionTypes.SET_WINNER, payload: theWinnerIs(shape, randomShape)}),
      5000  
    );
};