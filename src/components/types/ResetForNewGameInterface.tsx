import { ActionTypes } from "../constants/ActionTypes";


export interface ResetForNewGame {
    type: typeof ActionTypes.RESET_FOR_NEW_GAME 
  }