import { ActionTypes } from "../constants/ActionTypes";

export interface SetIsPanelOpen {
    type: typeof ActionTypes.SET_IS_PANEL_OPEN;
  }