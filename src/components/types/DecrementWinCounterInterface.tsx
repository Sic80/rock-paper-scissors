import { ActionTypes } from "../constants/ActionTypes";


export interface DecrementWinCounter {
    type: typeof ActionTypes.DECREMENT_WIN_COUNTER
}