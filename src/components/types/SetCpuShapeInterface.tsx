import { ActionTypes } from "../constants/ActionTypes";

export interface SetCpuShape {
    type: typeof ActionTypes.SET_CPU_SHAPE;
    payload: string;        
  }