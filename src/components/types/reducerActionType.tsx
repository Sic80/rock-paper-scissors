import { DecrementWinCounter } from "./DecrementWinCounterInterface";
import { IncrementWinCounter } from "./IncrementWinCounterInterface";
import { ResetForNewGame } from "./ResetForNewGameInterface";
import { SetCpuShape } from "./SetCpuShapeInterface";
import { SetIsLizardSpock } from "./SetIsLizardSpockInterface";
import { SetIsPanelOpen } from "./SetIsPanelOpenInterface";
import { SetPlayerShape } from "./SetPlayerShapeInterface";
import { SetWinner } from "./SetWinnerInterface";

   
   
   
export type Action = SetIsPanelOpen | SetPlayerShape | SetCpuShape | SetWinner | ResetForNewGame | SetIsLizardSpock | IncrementWinCounter | DecrementWinCounter;
     