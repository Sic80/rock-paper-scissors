import { ActionTypes } from "../constants/ActionTypes";


export interface IncrementWinCounter {
    type: typeof ActionTypes.INCREMENT_WIN_COUNTER;
}