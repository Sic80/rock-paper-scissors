import { ActionTypes } from "../constants/ActionTypes";

export interface SetWinner {
    type: typeof ActionTypes.SET_WINNER; 
    payload: string;        
  }