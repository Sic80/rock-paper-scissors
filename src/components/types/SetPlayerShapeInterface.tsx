import { ActionTypes } from "../constants/ActionTypes";


export interface SetPlayerShape {
    type: typeof ActionTypes.SET_PLAYER_SHAPE; 
    payload: string;
  }