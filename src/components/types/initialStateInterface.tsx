export interface InitialState {
    isPanelOpen: boolean,
    playerShape: string;
    cpuShape: string
    winner: null | string;
    winCounter: number;
    isLizardSpock: boolean;
};