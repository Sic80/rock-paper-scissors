import { ActionTypes } from "./constants/ActionTypes";
import { InitialState } from "./types/initialStateInterface";
import { Action } from "./types/reducerActionType";

export function reducer(state: InitialState, action: Action): InitialState {
    switch(action.type) {
        case ActionTypes.SET_IS_PANEL_OPEN:
            return {...state, isPanelOpen: !state.isPanelOpen};
        case ActionTypes.SET_PLAYER_SHAPE:
            return {...state, playerShape: action.payload};
        case ActionTypes.SET_CPU_SHAPE:
            return {...state, cpuShape: action.payload};
        case ActionTypes.SET_WINNER:
            return {...state, winner: action.payload};
        case ActionTypes.INCREMENT_WIN_COUNTER:
            return {...state, winCounter: state.winCounter + 1};
        case ActionTypes.DECREMENT_WIN_COUNTER:
            return {...state, winCounter: state.winCounter - 1};
        case ActionTypes.RESET_FOR_NEW_GAME:
            return {...state, playerShape: "", cpuShape: "", winner: null};
        case ActionTypes.SET_IS_LIZARD_SPOCK: 
            return {...state, isLizardSpock: !state.isLizardSpock};
        default:
            return state;

    }
}