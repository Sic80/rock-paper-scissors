import { useReducer } from 'react';
import 'react-sliding-side-panel/lib/index.css';
import TitleScoreHeader from './TiitleAndScoreBox';
import ChooseShapeTriangle from './ChooseShapeTriangle';
import ChooseShapePentagon from './ChooseShapePentagon';
import PlayerVsCpu from './PlayerVsCpu';
import RulesButtonAndPanel from './RulesButtonAndPanel';
import { InitialState } from './types/initialStateInterface';
import { reducer } from './Reducer';
import Switch from './Switch';
 

const Main: React.FunctionComponent = () => {   

    const initialState: InitialState = {isPanelOpen: false, playerShape: "", cpuShape: "", winner: null, winCounter: 0, isLizardSpock: false}; 
    const [state, dispatch] = useReducer(reducer, initialState);

    return (
        <div className="main" >
            <TitleScoreHeader state= {state} />
            <Switch dispatch={dispatch} />
            {state.playerShape === "" ? (
                state.isLizardSpock ? (
                    <ChooseShapePentagon dispatch={dispatch}/>
                    ) : (
                    <ChooseShapeTriangle dispatch={dispatch}/>  
                )
            ) : ( 
                <PlayerVsCpu 
                    state= {state}
                    dispatch={dispatch}
                />
            )}  
            <RulesButtonAndPanel 
                state= {state}
                dispatch={dispatch}
            /> 
       </div> 
     );
}
 
export default Main;