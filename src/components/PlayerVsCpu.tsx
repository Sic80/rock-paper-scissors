import ShapeButton from "./ShapeButton";
import { Dispatch } from 'react';
import { ReactComponent as Paper } from '../pic/icon-paper.svg';
import { ReactComponent as Scissors } from '../pic/icon-scissors.svg';
import { ReactComponent as Rock } from '../pic/icon-rock.svg';
import { ReactComponent as Lizard } from '../pic/icon-lizard.svg';
import { ReactComponent as Spock } from '../pic/icon-spock.svg';
import { Winner } from './constants/Winner';
import { ResetForNewGame } from "./types/ResetForNewGameInterface";
import { ActionTypes } from "./constants/ActionTypes";
import { InitialState } from "./types/initialStateInterface";

export interface PlayerVsCpuProps {
    state: InitialState;
    dispatch: Dispatch<ResetForNewGame>;
}
 
const PlayerVsCpu: React.FunctionComponent<PlayerVsCpuProps> = ({ state, dispatch}) => {
    
    const { winner, playerShape, cpuShape } = state;
    let winnerAnnouncement: string = winner === Winner.DRAW ? "DRAW" : winner === Winner.PLAYER ? "YOU WIN" : "YOU LOSE";
    const getShape = (shape: string) => {
        switch(shape) {
            case "paper":
                return <Paper viewBox="-20 -15 90 90" width="90%"/>;
            case "scissors":
                return <Scissors viewBox="-20 -15 90 90" width="90%"/>;
            case "rock":
                return <Rock viewBox="-20 -20 90 90" width="90%"/>;
            case "lizard":
                return <Lizard viewBox="-15 -15 90 90" width="90%"/>;
            case "spock":
                return <Spock viewBox="-27 -15 90 90" width="90%"/>;
            default:
                return <div>Loadin...</div>;
        }
    }

    return ( 
        <div>
            <div className={` ${winner ? 'container-with-result' : 'player-cpu_container'}`}>
                <div className="player-box">
                    <p className="player-text">YOU PICKED</p>
                    <div className={`player-shape ${(winner === Winner.PLAYER) && 'winner' }`}>
                        <ShapeButton 
                            child={getShape(playerShape)}
                            shapeName={playerShape}
                            size="xl"
                        />
                    </div>
                </div>
                <div className="cpu-box">
                    <p className="cpu-text">THE HOUSE PICKED</p>
                    <div className={`cpu-shape ${(winner === Winner.CPU) && 'winner' }`}>
                        {cpuShape === "" ? (
                            <div className="empty-shape">
                            </div>
                        ) : (
                            <ShapeButton                                
                                child={getShape(cpuShape)}
                                shapeName={cpuShape}
                                size="xl"
                            />
                        )} 
                    </div>
                </div> 
                {winner &&
                    <div className="winner-box"> 
                        <p className="winner-text">
                            {winnerAnnouncement}
                        </p>
                        <button
                            className="play-again-button"
                            onClick={() => dispatch({type: ActionTypes.RESET_FOR_NEW_GAME})}
                        >
                            PLAY AGAIN
                        </button>          
                    </div>
                }
            </div>
        </div>
     );
}
 
export default PlayerVsCpu;