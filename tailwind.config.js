const defaultTheme = require('tailwindcss/defaultTheme')


module.exports = {
  purge: [],
  darkMode: false, // or 'media' or 'class'
  theme: {
      screens: {
        'xs': '400px',
        ...defaultTheme.screens,
      },
    extend: {
      colors: {
        "blue-primary":'#1D3050',
        "blue-secondary": "#5D718A",
        "dark-blue": "#192845"
      },
      borderWidth: {
       '12': '12px',
      },
      height: {
        '22': '5.5rem',
        '30': '7.4rem',
        '90': '22rem'
      },
      width: {
        '22': '5.5rem',
        '30': '7.4rem'

      },
      backgroundSize: {
        '48': '12.5rem',
        '60': '15rem',
        '64': '16rem',
        '72': '18rem'

      }
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
